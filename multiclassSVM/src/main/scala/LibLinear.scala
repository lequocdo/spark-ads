import org.apache.spark.SparkContext  
import org.apache.spark.SparkContext._  
import org.apache.spark.SparkConf  
import org.apache.spark.SparkContext  
import tw.edu.ntu.csie.liblinear._  


/**
 * SE Group TU Dresden, Germany
 * @lequocdo
 */
        
object LibLinear {  
    def main(args:Array[String]) {  
        val logFile = "/Kdd99-train"  
        val conf = new SparkConf().setAppName("Spark-LIBLINEAR")  
        val sc = new SparkContext(conf)  
        val add_liblinear = sc.addJar("/home/ubuntu/spark/install/apps/linearsvm/libsvm/distributedlibsvm/spark-liblinear/lib/spark-liblinear_2.10-1.96.jar")
        val add_jblas = sc.addJar("/home/ubuntu/spark/install/apps/linearsvm/libsvm/distributedlibsvm/spark-liblinear/lib/jblas-1.2.3.jar")
        val data = Utils.loadLibSVMData(sc, logFile)  
              
        //train model  
        val model = SparkLiblinear.train(data, "-c 4 -e 0.00035 ")  
          
        model.saveModel("/tmp/SVMLibLinearModel")   

 
        //Test model
        val test_data = Utils.loadLibSVMData(sc, "/Kdd99-test")

        val LabelAndPreds = test_data.map { point =>  
            val prediction = model.predict(point)  
            (point.y, prediction)  
        }  
      
        val accuracy = LabelAndPreds.filter(r => r._1 == r._2).count.toDouble / test_data.count  
     
        println("Training Accuracy = " + accuracy)  
      
    }  
}  
