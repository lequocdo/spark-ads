\section{Implementation}
\label{sec:implementation}

\begin{figure}[htp]
\centering
\includegraphics[width=0.40\textwidth]{figures/dk2-explaination.png}
\caption{dK-2 distance between consecutive graphs implementation using Map, Reduce and Join.}
\label{fig:dk2-explaination}
\end{figure}

In this section, we present our implementation of each component of the proposed architecture using Spark (version 1.4.0).

\subsection{Network Flow Collector}
We extended NGMON \cite{ngmon} to develop a network flow exporter tool that is used to capture network traffic from network backbone and export to network flows. The tool also could be used to analyze network packets from pcap traces and convert them to flows data.

\subsection{Network Flow Storage/Queue}
In this work, network flow data is exported from {\em Network Flow Collector} every $t$ seconds ($t = 5, 10, 30, 60, 120, ...$ seconds) to present near- real-time view of network communications. Since the proposed system targets large-scale network traffic, storing network flow data is necessary to be scalable. To meet the requirement, HDFS and Tachyon are used for {\em off-line} analysis and Kafka is used for {\em on-line} analysis.

\textbf{Hadoop File System (HDFS).} HDFS is an open source implementation of Google File System \cite{gfs} that provides distributed, scalable, and reliable storage. It distributes stored data across multiple nodes. In HDFS, the stored data is represented as files that are split into blocks or chunks that are distributed across nodes. Traditional HDFS could be accelerated by Tachyon \cite{tachyon}.

\textbf{Tachyon.} Tachyon is an in-memory, fault-tolerant, distributed file system which provides reliable data sharing at memory speed across cluster computing framework \cite{tachyon}. Tachyon applies \textit{lineage} concept of Spark to achieve memory throughput of read and write operations without compromising fault-tolerance. Tachyon is experimentally proven to outperform in-memory HDFS by 110x for write operations \cite{tachyon}. In our work, network flow data is pushed directly to Tachyon file system to gain the benefit of in-memory computing enabling low latency of the proposed framework.

\textbf{Kafka.} Kafka\footnote{http://kafka.apache.org} is a distributed message queueing framework that provides a reliable, high-throughput, low-latency system of queueing real-time data streams. In the proposed architecture, network flow data is ingested to Kafka for real-time processing using Spark streaming component. 

\subsection{Scalable Network Traffic Anomaly Detection}
\textbf{Graph based mechanism using GraphX.}
In this approach, network traffic is exported to network flow data and converted network flow data to graphs with TDG model every $t\ seconds$ by using the  {\em Network Flow Collector}.
The graphs are presented in edges format that is readable by Spark GraphX. The edges format also reduces required memory space to store the graphs. After that, the graphs are processed using GraphX. Since GraphX library did not provide a function to calculate joint degree distribution of a graph, we implemented the function\footnote{http://bitbucket.org/lequocdo/spark-ads} to compute dK-2 distance metrics.

Figure \ref{fig:dk2-explaination} presents an example how to use {\em Map}, {\em Reduce} and {\em Join} operations in Spark to implement our proposed algorithm. First, the degree distribution of each graph is calculated and presented as an RDD. Next,  {\em Map} and {\em Reduce} operations is applied on the RDD to identify joint degree distribution of each graph. After that, the joint degree distribution RDDs of two graphs are joined by using {\em Join} operation, then the Euclidean distance between them is determined by applying  {\em Map} and {\em Reduce} operations one more time on the joined RDD. By analyzing the dK-2 distance metric over time, we can detect anomalies in large-scale network flow datasets.

\textbf{SVMs based mechanism using MLlib.}
Fortunately, Linear SVM is implemented in Spark MLlib. What we only needed to do is that we developed a pre-processing program to convert the network flow data to LibSVM~\cite{libsvm} format, since datasets in this format is readable by Spark MLlib. However, the SVM implementation of latest version of Spark MLlib supports only binary classification, not multi-classes classification. We extended the linear SVM module of Spark MLlib  to support multi-classes classification, since in some case we want to identify attacks causes of detected anomalies.

\subsection{Spark Performance Tuning}
Spark can cache intermediate results across the distributed memory of distributed workers to reduce latency. To avoid recomputing RDDs many times in iterative algorithms, the RDDs should be cached or persisted. For example, in the implementation for calculating dK-2 distance metrics, the RDDs of degree distribution of graphs are cached since they are used many times. In addition, some configuration parameters of Spark need to be tuned to improve performance. For example, the number of partitions of data should be at least double the number of cores in the cluster to gain benefit in parallel computing. To deal with stragglers caused by some nodes being slower than others, a speculation parameter should be turned on \textit{conf.set("spark.speculation", "true")}. This setting will copy slow tasks on a straggler node to another node.