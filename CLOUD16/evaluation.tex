\section{Evaluation}
\label{sec:evaluation}

In this section, we evaluate the performance of \sad~along several key metrics such as scalability and accuracy. We split this
evaluation in two subsections. First, we evaluate \sad~in detecting anomalies using the graph based mechanism with several real datasets. Then, we report several experimental results where we use \sad~with the distributed SVMs mechanism to detect anomalies.

\textbf{Experimental setup.} We run experiments using a cluster of $12$ nodes connected via Gigabit Ethernet (1000BaseT full duplex). Each node contains $2$ Intel Xeon E5405 CPUs (quad core), $8$ GB of RAM and hard disks are attached via SATA-2.

\subsection{Graph-based Large-scale Network Traffic Anomaly Detection}
We evaluate \sad~with the graph based mechanism (described in Section \ref{graph}) in processing three datasets including  DDoS 7.7, synthesized P2P botnet traffic, and  CAIDA anonymised Internet traces.

\textbf{POSTECH - DDos 7.7.}
This trace contains traffic of a famous DDoS attack on July 7, 2009. The POSTECH trace was captured during one hour of activity. During July 2009, major government and commercial websites in South Korea were subjected to heavy DDoS attacks which, according to intelligence agencies in South Korea and the United States, were probably launched by a special cyber warfare unit belonging to North Korean Army. At that time, many zombie computers in POSTECH campus network were used fro the DDoS attack. We use the proposed framework to analyze the trace, dK-2 distance metric changes dramatically in the $2^{nd}$ , $22^{nd}$ and $23^{rd}$ minute (red circles) of the trace (Fig. \ref{fig:dk2-postech}). In fact, a botnet attack occurred in the network traffic of POSTECH during these periods. This trace could be analyzed using our previous implementation \cite{tdg2}, however this implementation cannot process large-scale network traffic data.

\begin{figure}[htp]
\centering
\includegraphics[width=0.42\textwidth]{plot/dk2-tcp-Postech.pdf}
\caption{dK-2 distance value over time of POSTECH trace.}
\label{fig:dk2-postech}
\end{figure}

\textbf{Synthesized Traffic Dataset.} 
\begin{figure}[htp]
\centering
\includegraphics[width=0.45\textwidth]{figures/Peacon-Postech.png}
\caption{Statistical metrics and dK-2 distance metric over time of the synthesized trace and example of TDGs for both anomalous and normal periods in the synthesized trace.}
\label{fig:peacon}
\end{figure}
To validate the ability of our method to detect P2P botnet communications, we injected P2P botnet (peacomm) trace into normal POSTECH traffic trace. We selected a time point at the $29^{th}$ minute of one hour trace as the time we attempt to detect our synthesized botnet. In order to generate the traffic trace representative of real P2P botnet traffic, we executed Trojan peacomm binary files in a honeynet which consisted of $12$ hosts. The Peacomm botnet is one of the botnets implemented as a full P2P network \cite{p2pbotnet}. Peacomm uses Overnet P2P protocol for communication among peers (bots). The Overnet P2P protocol is a decentralized peer network which implements a distributed hash table (DHT) based on the Kademlia algorithm \cite{p2pbotnet}. A peer (bot) in Peacomm first communicates with other peers using a hardcoded set of initial peers, then searches for an encrypted URL to download additional components (crimeware). The crimeware consists of a SMTP email spamming component that is used to send emails from the infected host, and an address harvester is used to collect all email addresses located in the  infected host and send them back to the bot master. We used the honeypots to obtain the communications between the various bots while the botnet was in operation. Then, we merged this P2P botnet communication traffic with the POSTECH normal traffic in order to create the botnet test dataset.

Detecting communication between bots in P2P botnets is not a trivial task since in the P2P botnet architecture there is no central bot. To receive and distribute command messages, each P2P bot interacts with a small subset of the bots and maintains its own peer list. In addition, communications between the bots consume little bandwidth, so statistical features based on the number of packets, packet sizes, number of flows and etc. cannot be used to detect P2P botnet communications. In this experiment, we used the proposed approach to detect the communication of P2P botnet. Figure \ref{fig:peacon} shows a more detailed view of the communication patterns around the time periods of sudden increase of dK-2 distance value. At the point when the dK-2 distance value suddenly increased, we can observe that a new communication cluster appeared. By observing the cluster, synchronous scanning activities from multiple hosts to the same subnet were revealed. For comparison, we also represent a time series of the volume metrics of the same data (Fig. \ref{fig:peacon}). The figure indicates that there is no sudden change in the size or the number of flows. The statistical metrics cannot be used to detect such anomalies. However, the dK-2 distance metric is effective for detecting the anomalies.

\textbf{CAIDA anonymised Internet traces.} 
To evaluate the capacity of \sad~with the graph-based mechanism in processing large-scale network traffic data, we used real traffic traces from CAIDA anonymised Internet traces \cite{caida2015}. These traces contain anonymised passive traffic captured from CAIDA’s equinix-chicago on high-speed Internet backbone links, and is mainly used for research of the characteristics of Internet traffic. The traffic traces used in this paper were captured in 2015 from high-speed Internet backbone links in Chicago (labeled as A) containing around $670$ GB of network traffic. In this work, we focus only on analyzing TCP flows in the datasets. Figure \ref{fig:dk2-caida} depicts the dK-2 distance metric values of TCP flow data of the CAIDA trace. 

\begin{figure}[htp]
\centering
\includegraphics[width=0.42\textwidth]{plot/dk2-tcp-CaiDa.pdf}
\caption{dK-2 distance value over time of CAIDA trace.}
\label{fig:dk2-caida}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[width=0.42\textwidth]{plot/dk2-scalability-30mins-graph.pdf}
\caption{Scalability in processing CAIDA trace.}
\label{fig:dk-scalability}
\end{figure}

\begin{figure}
\centering    
\subfigure[CPU Utilization]{\label{fig:a}\includegraphics[width=0.48\linewidth]{plot/CPUSVM-4nodes.pdf}}
\subfigure[Memory Utilization]{\label{fig:b}\includegraphics[width=0.48\linewidth]{plot/RAMSVM-4nodes.pdf}}
\caption{CPU and Memory Utilization in Processing Anonymized Internet Traces in 2015.}
\label{fig:dk-ram-cpu}
\end{figure}


Figure \ref{fig:dk-scalability} presents the scalability of the proposed framework in processing the dataset. The dK-2 with Spark scales well until $6$ nodes since from 8 nodes the data input for each worker in the Spark system is small, so the time to process the data becomes less than the time to start the stages plus the shuffle time.

Figure \ref{fig:dk-ram-cpu} represents the CPU utilization and memory utilization of 4 worker nodes during calculation of dK-2 distance of two consecutive graphs that represent two consecutive 30 minutes of the Internet traces. The workload is well distributed among workers using the proposed approach. In addition, if this dataset is analyzed using our previous solution \cite{tdg2}, designed for single server, on a machine that contains $8$ cores and $15$~GB of RAM, the \textit{OutOfMemory Error} will occur in this case. In other words, the previous centralized approach cannot process the large-scale datasets meanwhile the proposed distributed approach in this paper can effectively handle the large-scale network flow data.

\subsection{ Large-scale Network Traffic Anomaly Detection using Distributed Support Vector Machines}
\begin{figure}[htp]
\centering
\includegraphics[width=0.42\textwidth]{plot/svm-scalability.pdf}
\caption{Scalability in the training process of KDD Cup 99 dataset.}
\label{fig:svm-scalability}
\end{figure}

To demonstrate the ability of the distributed SVMs based anomaly detection approach, we used KDD Cup 99 dataset.
\textbf{KDD Cup 1999 DataSets.}
The datasets contains about $708$~MB with about $4.9$~M connections \cite{sparkbook2}. This datasets have a total of $24$ training attack types~\footnote{{https://kdd.ics.uci.edu/databases/kddcup99/training\_attack\_types}}. Each connection in the datasets contains the information such as the number of bytes sent, login attempts, TCP errors, and etc.  Each connection is a record in CSV-formatted data, containing 38 features \cite{sparkbook2}. This is not massive or large datases, but it is good enough to evaluate the proposed approach. To use the datasets, we developed a pre-processing program to convert the datasets into LibSVM format to process it by using Spark MLlib. We labeled the attack connections with {\em label 1}, normal connections with {\em label 0}.

Figure \ref{fig:svm-scalability} depicts the scalability of the proposed framework during the training processing of the dataset. With 12 nodes, the system can finish the training process in $40s$. If we use the traditional SVM training using LibSVM \cite{libsvm} with 1 node, the processing is $204m$ and $44s$. The accuracy of the classification using the traditional libsvm is $91.9\%$, whereas the accuracy of the classification using our proposed framework is $91.5\%$ with $100$ iterations.