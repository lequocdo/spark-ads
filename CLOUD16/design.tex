\section{Proposed Approach}
\label{sec:design}
In this section, we present the proposed architecture and design details of \sad, a salable network traffic anomaly detection framework using Spark.

\subsection{Architecture}
\begin{figure}[htp]
\centering
\includegraphics[width=0.42\textwidth]{figures/ads-architecture.png}
\caption{Network Traffic Analysis Architecture with Spark.}
\label{fig:sparknetflow}
\end{figure}

One of the goals of the proposed architecture is to improve performance in anomaly detection of large-scale network traffic as well as to maximize reusability and compatibility with the Spark platform. Figure \ref{fig:sparknetflow} depicts a high-level of our proposed architecture for network traffic anomaly detection. The architecture contains three core components including Network Flow Collector, Network Flow Storage, and Analysis Engine. 

\subsubsection{Network Flow Collector}
The network flow collector receives network flow data (NetFlow) from probe systems or network devices, then redistributes them to {\em Network Flow Storage} component, that is a persistent storage such as  S3, HDFS, or Tachyon \cite{tachyon}, for {\em off-line} analysis or a Pub/Sub or queue system (e.g. Kafka) for {\em on-line} analysis. The network flow collector is designed to work in a distributed way that can scale-out and scale-up to handle the changing of network flow data input over time.

\subsubsection{Network Flow Storage/Queue}
One of the goals of our design is that the architecture should support both batch and stream processing to analyze the large-scale network traffic data. Therefore, {\em Network Flow Storage} is designed to store historical network flow data and {\em Network Flow Queue} is provided to support real-time processing the data. 

\subsubsection{Network Flow Analysis}
Once the network flow data is available from the storage, the analysis component can be executed on the cluster. This component employed two integrated modules of Spark namely GraphX and MLlib, that are used to analyze the large-scale network flow data to detect anomalies. The analysis results are stored in the storage component or in a large-scale database system such as Cassandra\footnote{http://www.datastax.com/products/datastax-enterprise-production-certified-cassandra} that provides the query interface. 

%In this paper, we only focus on using GraphX to detect anomalies in the large-scale network flow data.

\subsection{Graph-based Network Traffic Anomaly Detection}
\label{graph}
\subsubsection{Network Traffic Modeling Using Graphs}

To analyze the network flow data using graph-based methods, we use traffic dispersion graph (TDG)~\cite{tdg} \cite{tdg2} to model network communication from the network flow data. A TDG is defined as a graphical representation of the various interactions ({\em ``who talks to whom''}) of a group of nodes. In IP networks, a node of the TDG corresponds to an entity with a distinct IP address and a flow record in the network flow data is an edge between sender node and receiver node \cite{tdg}. TDG is used to represent communication patterns between nodes of a computer network. It provides a graph-based way of analyzing network traffic with a powerful visualization. 

\subsubsection{Anomaly Detection}
\begin{figure}[htp]
\centering
\includegraphics[width=0.42\textwidth]{figures/spark-dk2.png}
\caption{Graph-based network traffic anomaly detection.}
\label{fig:spark-dk2} 
\end{figure}   


To detect anomalies in network traffic, network flow data is collected every $t$ seconds ($t = 5, 10, 30, 60, 120, ...$). The network data flow in each $t$ seconds is converted to a graph using TDG. The anomalies could be detected by measuring the similarity or difference between two consecutive graphs (Fig. \ref{fig:spark-dk2}). 

To measure the difference between consecutive graphs, the dK-2 distance metric is used \cite{tdg2}. It is based on the graph similarity metric which is built upon the so-called dK-series in \cite{dk-series}. The dK-series were applied to quantify the amount of correlation present in real-world graphs~\cite{dk-graph}. In the dK-series definition, dK-0 describes the average degree $\bar{k}$, dK-1 the node degree distribution $P(k)$, dK-2 the joint degree distribution (JDD), $P(k_1, k_2)$, dK-d ($d \geq 3$) is the order-d distribution $P_{d}$ ($P_{d}$ represents the interaction between d-nodes with degree $k_1, k_2,..., k_{d}$ in a graph). In addition, the set of graphs having the same distribution $P(k)$ (denoted as dK-1 graphs) is a subset of the set of dK-0 graphs, the set of dK-2 graphs is a subset of the set of dK-1 graphs and the set of dK-d graphs is a subset of the set of dK-(d-1) graphs \cite{dk-graph}.

dK-d is able to capture increasingly complex graph properties with increasing $d$ (the dK-series can capture more graph structure details than other metrics). The reason for using dK-2 series is that dK-2 requires lower computational complexity than dK-d for $d \geq 3$ and can represent more complex structure patterns in graphs than dK-0 and dK-1. The dK-2 distance (the difference) of two graphs, $G$ and $G'$, is defined as the {\em Euclidean distance} between the corresponding joint degree distributions $P(k_1, k_2)$ and $P'(k_1, k_2)$. This approach was introduced in our previous work \cite{tdg2}.  However, the previous implementation cannot handle large-scale datasets, since it runs out of memory in a single server. In this paper, we present a new implementation of this approach that is able to cope with large-scale network flow datasets.

Beside the graph based mechanism, we also propose a machine learning based method, in particular, support vector machines (SVM), to detect anomalies in large-scale network traffic data. The next section covers with details how SVMs are used to detect anomalies.

\subsection{Machine Learning based Network Traffic Anomaly Detection}
\label{machine}
\begin{figure}[htp]
\centering
\includegraphics[width=0.40\textwidth]{figures/binSVM.pdf}
\caption{SVM binary classification.}
\label{fig:binsvm} 
\end{figure}

 There are many machine learning techniques that could be used to analyze network traffic. However, in this paper, SVM  is selected to demonstrate how to use Spark MLlib to analyze the network flow datasets.

\subsubsection{Support Vector Machines}
\label{sect:svm}

SVM is a supervised learning method for classification and regression analysis. The SVM has been extensively used for network traffic analysis \cite{svmtcp} \cite{kim2008}. To present the theory behind SVMs, in this section we examine binary classification using SVMs. Given a set of training data $\mathcal{D} = {( \mathbf{x}_i,y_i)} |\mathbf{x}_i \in \Re^d,$ $i = 1...n,$ where $\mathbf{x}_i$ is a $d$-dimension observation vector, $y_i \in {-1,1}$ is the class label of $\mathbf{x}_i$, and $n$ is size of $\mathcal{D}$. During training, SVM will find a hyperplane that maximizes the margin between the two classes of the training data in $\mathcal{D}$ to separate the two classes (as known as $+1$ and $-1$). If $\mathbf{x^{*}}$ lie on this plane, it will satisfy:
\begin{equation}
\mathbf{w}^T\mathbf{x^{*}}+b=0
\end{equation}
where $\mathbf{w}$ is the weighting vector of the plane and $b$ is a scalar. The binary classification is performed by verifying on what side of the hyperplane a data point, $\mathbf{x}$, lies by:
\begin{equation}
\label{svmclassify}
\operatorname{sign}(\mathbf{w}^T\mathbf{x}+b)
\end{equation}

The labeling of data point $x$ would be the geometric distance between $x$ and the decision boundary (Fig. \ref{fig:binsvm}). During training of the SVM we are trying to find the hyperplane that separates the training data. If there exist more than one plane that separates the classes, the optimal separating plane has the largest geometric distance from all data points in the training set (Fig. \ref{fig:binsvm}). To do this, we minimize: $\dfrac{1}{2}\|\mathbf{w}\|^2$ subject to the constraints $y_i(\mathbf{w}^T\mathbf{x}+b) \geq 1$. This leads to a Lagrangian optimization problem:
\begin{equation}
\label{lagrangian}
\L=\dfrac{1}{2}\|\mathbf{w}\|^2 - \sum_{i=1}^{n}\alpha_{i}(y_{i}(\mathbf{w}^T\mathbf{x}_{i}+b)-1)
\end{equation}

This problem can be rewritten as following quadratic optimization problem:

\begin{equation}
\label{quadratic}
\min \ \mathcal{P}(\mathbf{w}, b, \xi) = \dfrac{1}{2}\|\mathbf{w}\|^2 + C\sum_{i=1}^{n}\xi_{i} 
\end{equation}

\begin{equation*} 
s.t. \ \ y_{i}(\mathbf{w}^T\mathbf{x}_{i}+b) \geq 1 - \xi_{i}, \xi_{i} >0
\end{equation*}

where $C > 0$ is the penalty or regularization parameter of the training errors. $\xi_{i}$ are slack variable:

\begin{equation*} 
\xi_{i} = \left\lbrace \begin{array} {l l} 0\ , if \ y_{i}(\mathbf{w}^T\mathbf{x}_{i}+b) \geq 1 \\ \\ 1 - y_{i}(\mathbf{w}^T\mathbf{x}_{i}+b)\ , if \ y_{i}(\mathbf{w}^T\mathbf{x}_{i}+b) < 1  \end{array} \right.
\end{equation*}

For some types of data, there might not be any plane that can be used to separate the two classes. To solve such problems, nonlinear SVMs are used. The idea behind nonlinear SVMs is to transform data to new space where a linear classifier is good enough to classify them, instead of using a nonlinear classifier in the problem space.

%In practice, Sequential Minimal Optimization (SMO) \cite{smo} is a popular algorithm  to solve the SVM problem. For some types of data, there might not be any plane that can be used to separate the two classes. To solve such problems, nonlinear SVMs are used. The idea behind nonlinear SVMs is to transform data to new space where a linear classifier is good enough to classify them, instead of using a nonlinear classifier in the problem space. To do that, a map function $\Phi(x)$ is used to transform training data points into a different (usually higher) dimensional space where the data can be separable using a linear classifier. However, if the space has a large dimension, then finding the mapping is challenging. SVMs resolves this issue by using Kernels represented as $K(x_{i}, x_{j}) = \Phi(x_{i}).\Phi(x_{j})$ because the training algorithm depends on the dot product $\Phi(x_{i}).\Phi(x_{j})$. $K$ is used in the training algorithm without explicitly using the mapping $\Phi(x)$.

In the current version of Spark MLlib, it supports only the linear SVMs, but not the kernel or non-linear SVMs, since the non-linear SVMs do not scale well. In the next section, we explain how to apply the linear distributed SVMs  to detect anomalies in large-scale network traffic datasets. 

 %The growth of the Internet traffic in both of volume and complexity, demonstrates the notion of Big Data. This obstacles to apply SVMs in processing big network traffic data, since the SVMs runtime scales approximately cubically with the number of observations in the large training datasets \cite{tsang2005}. In addition, the large datasets do not fit into memory and even in the hard disk of a single machine.  To overcome the problems of Big Data, the parallel and distributed methods for SVM training have been intensively studied. To reduce the time spent in kernel SVM training on large-scale datasets, Graf \textit{et al.} \cite{cascadesvm} introduced Cascade SVM which is a multilevel approach. In this approach, large-scale datasets are partitioned into manageable subsets, so each subset can be fitted and trained in a single machine. Once the local models are built, they are combined in a manner that guarantees optimality after several iterations. 

\subsubsection{Anomaly Detection}
\begin{figure}[htp]
\centering
\includegraphics[width=0.42\textwidth]{figures/spark-svm.png}
\caption{SVMs based Network Traffic Anomaly Detection.}
\label{fig:spark-svm} 
\end{figure}

Figure \ref{fig:spark-svm} illustrates how SVM is applied to analyze large-scale network flow data.  A feature extraction module is used to extract features that are used  for the SVM classification. Each flow record is represented with a set of features that are selected to ensure carrying enough information to identify anomalies.

 In the training phase, the features are extracted from a dataset to create a training dataset. Each flow with the features is labeled as a normal or abnormal record, or even with attack types in the training dataset. The training dataset is used as input of SVM training process. A classifying model is built after the training process. In the testing phase, the model is used to classify network flow data from many sources as normal, abnormal flows or with attack types.